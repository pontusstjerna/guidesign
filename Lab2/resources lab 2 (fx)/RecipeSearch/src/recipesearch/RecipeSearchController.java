
package recipesearch;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringJoiner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import se.chalmers.ait.dat215.lab2.Ingredient;
import se.chalmers.ait.dat215.lab2.Recipe;
import se.chalmers.ait.dat215.lab2.RecipeDatabase;
import se.chalmers.ait.dat215.lab2.SearchFilter;

public class RecipeSearchController implements Initializable {

    private enum sortBys {Relevance, Price, Name, Time}

    private List<Recipe> searchedResult;

    @FXML private Button searchButton;

    @FXML private MenuBar menuBar;
    @FXML private TextField txtFoodSearch;

    @FXML private ComboBox<String> cmbxCuisine;
    @FXML private ComboBox<String> cmbxMainIngredient;
    @FXML private ComboBox<String> cmbxDifficulty;
    @FXML private ComboBox<sortBys> cmbxSort;

    @FXML private Slider sliderPrice;
    @FXML private Slider sliderTime;

    @FXML private Label priceLabel;
    @FXML private Label timeLabel;

    @FXML private ListView listViewResults;

    @FXML private Text txtCuisine;
    @FXML private Text txtMainIngredient;
    @FXML private Text txtDifficulty;
    @FXML private Text txtPrice;
    @FXML private Text txtTime;

    @FXML private Label txtRecipeLabel;
    @FXML private ListView<String> listViewIngredients;
    @FXML private TextArea txtDescription;
    @FXML private Label txtServings;

    @FXML private ImageView imageViewRecipe;
    @FXML private BorderPane borderPaneMain;


    private String priceLabelText;
    private String timeLabelText;
    private int priceValue;

    private RecipeDatabase database;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        populateData();


        priceLabelText = priceLabel.getText();
        timeLabelText = timeLabel.getText();
        priceValue = (int) sliderPrice.getValue() * 10;

        //To get max price: Find all recipes, choose the most expensive
        sliderPrice.valueProperty().addListener(e -> priceLabel.setText(priceLabelText
                + " " + (int)sliderPrice.getValue()));


        sliderTime.valueProperty().addListener(x -> {
            sliderTime.setValue(Math.round(sliderTime.getValue()/10) * 10);
            timeLabel.setText(timeLabelText + " " + (int)sliderTime.getValue());

        });

        cmbxSort.valueProperty().addListener(e -> sortby(cmbxSort.getValue()));

        listViewResults.getSelectionModel().selectedItemProperty().addListener(x -> {
            int i = listViewResults.getSelectionModel().getSelectedIndex();
            if (i == -1)
                borderPaneMain.setVisible(false);
            else
            showRecipe(searchedResult.get(i));
        });

        database = RecipeDatabase.getSharedInstance();
        searchedResult = database.search(new SearchFilter("",0,"",0,""));

        sliderPrice.setMax(getMostExpensive(database));
        sliderPrice.setValue(sliderPrice.getMax());
        sliderPrice.setMin(getCheapest(database));
    }
    
    @FXML 
    protected void openAboutActionPerformed(ActionEvent event) throws IOException{
    
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("recipesearch/resources/RecipeSearch");
        Parent root = FXMLLoader.load(getClass().getResource("recipe_search_about.fxml"), bundle);
        Stage aboutStage = new Stage();
        aboutStage.setScene(new Scene(root));
        aboutStage.setTitle(bundle.getString("about.title.text"));
        aboutStage.initModality(Modality.APPLICATION_MODAL);
        aboutStage.setResizable(false);
        aboutStage.showAndWait();
    }
    
    @FXML 
    protected void closeApplicationActionPerformed(ActionEvent event) throws IOException{
        Stage addressBookStage = (Stage) menuBar.getScene().getWindow();
        addressBookStage.hide();
    }

    @FXML
    protected void searchClickedPerformed(ActionEvent event) throws IOException, ClassNotFoundException{
        search();
    }

    private List<Recipe> filterByName(List<Recipe> unfiltered, String name){
        List<Recipe> res = new ArrayList<>();
        for(int i = 0; i < unfiltered.size(); i++){
            if(unfiltered.get(i).getName().toLowerCase().contains(name.toLowerCase()))
                res.add(unfiltered.get(i));
        }
        return res;
    }

    private String[] listToArrayString(List<? extends Recipe> list){
        String[] res = new String[list.size()];
        for (int i = 0; i < list.size(); i++){
            res[i] = list.get(i).getName();
        }

        return res;
    }

    //Jag gjorde den här Alesksksks
    private void populateData()
    {
        String[] cuisines = {"Sverige", "Grekland", "Indien", "Asien", "Afrika"};
        cmbxCuisine.getItems().addAll(cuisines);

        String[] mainIngreds = {"Kött", "Fisk", "Kyckling"};
        cmbxMainIngredient.getItems().addAll(mainIngreds);

        String[] difficulties = {"Lätt", "Mellan", "Svår"};
        cmbxDifficulty.getItems().addAll(difficulties);

        sortBys[] sorts = {sortBys.Relevance, sortBys.Name, sortBys.Time, sortBys.Price};
        cmbxSort.getItems().addAll(sorts);
        cmbxSort.setValue(sortBys.Relevance);
    }

    private int getMostExpensive(RecipeDatabase db){
        int max = 0;
        for(Recipe r : searchedResult) {
            if(r.getPrice() > max)
                max = r.getPrice();
        }

        return max;
    }

    private int getCheapest(RecipeDatabase db){
        int min = 0;
        for(Recipe r : searchedResult) {
            if(r.getPrice() < min)
                min = r.getPrice();
        }

        return min;
    }

    @FXML
    protected void txtSearchOnKeyPressed(KeyEvent e){
        if(e.getCode() == KeyCode.ENTER){
            search();
        }
    }

    private void search(){
        SearchFilter searchFilter = new SearchFilter(cmbxDifficulty.getValue(), (int)sliderTime.getValue(),
                cmbxCuisine.getValue(),(int)sliderPrice.getValue(), cmbxMainIngredient.getValue());

        searchedResult = filterByName(database.search(searchFilter), txtFoodSearch.getText());

        listViewResults.getItems().clear();
        listViewResults.getItems().addAll(listToArrayString(searchedResult));

        cmbxSort.setValue(cmbxSort.getItems().get(0));

        if(searchedResult.size() == 0)
            listViewResults.getItems().add("No results... :'(");

    }

    private void sortby(sortBys sortBy){
        switch (sortBy){
            case Relevance:
                break;
            case Price:
                searchedResult.sort((
                        Recipe r1, Recipe r2) -> r1.getPrice() < r2.getPrice() ? -1 : 1);
                break;
            case Name:
                searchedResult.sort((
                        Recipe r1, Recipe r2) -> r1.getName().compareTo(r2.getName()));
                break;
            case Time:
                searchedResult.sort((
                        Recipe r1, Recipe r2) -> r1.getTime() < r2.getTime() ? -1 : 1);
                break;
            default:
                break;
        }
        listViewResults.getItems().clear();
        listViewResults.getItems().addAll(listToArrayString(searchedResult));
    }

    private void showRecipe(Recipe selected) {
        borderPaneMain.setVisible(true);

        txtCuisine.setText(selected.getCuisine());
        txtDifficulty.setText(selected.getDifficulty());
        txtMainIngredient.setText(selected.getMainIngredient());
        txtPrice.setText(String.valueOf(selected.getPrice()));
        txtTime.setText(String.valueOf(selected.getTime()));

        txtRecipeLabel.setText(selected.getName());

        listViewIngredients.getItems().clear();
        for(Ingredient i : selected.getIngredients())
            listViewIngredients.getItems().add(i.getName() + " " + i.getAmount() + " " + i.getUnit());

        txtDescription.setText(selected.getInstruction());
        txtServings.setText(String.valueOf(selected.getServings()) + " servings");

        imageViewRecipe.setImage(selected.getFXImage());
    }
}
