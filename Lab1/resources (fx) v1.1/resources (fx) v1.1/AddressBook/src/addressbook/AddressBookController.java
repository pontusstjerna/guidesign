
package addressbook;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import se.chalmers.cse.dat215.lab1.Presenter;


public class AddressBookController implements Initializable {
    
    @FXML private MenuBar menuBar;
    @FXML private Button newButton;
    @FXML private Button deleteButton;
    @FXML private ListView listView;

    //Menu items



    //Text fields
    @FXML private TextField txtFirstName;
    @FXML private TextField txtLastName;
    @FXML private TextField txtPhone;
    @FXML private TextField txtEmail;
    @FXML private TextField txtAddress;
    @FXML private TextField txtPostCode;
    @FXML private TextField txtCity;

    private Presenter presenter;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        presenter = new Presenter(
                listView,
                txtFirstName,
                txtLastName,
                txtPhone,
                txtEmail,
                txtAddress,
                txtPostCode,
                txtCity);

        listView.getSelectionModel().selectedItemProperty().addListener(x -> presenter.contactsListChanged());

        txtFirstName.focusedProperty().addListener(new TextFieldListener(txtFirstName));
        txtLastName.focusedProperty().addListener(new TextFieldListener(txtLastName));
        txtPhone.focusedProperty().addListener(new TextFieldListener(txtPhone));
        txtEmail.focusedProperty().addListener(new TextFieldListener(txtEmail));
        txtAddress.focusedProperty().addListener(new TextFieldListener(txtAddress));
        txtPostCode.focusedProperty().addListener(new TextFieldListener(txtPostCode));
        txtCity.focusedProperty().addListener(new TextFieldListener(txtCity));



        presenter.init();
    }
    
    @FXML 
    protected void openAboutActionPerformed(ActionEvent event) throws IOException{
    
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("addressbook/resources/AddressBook");
        Parent root = FXMLLoader.load(getClass().getResource("address_book_about.fxml"), bundle);
        Stage aboutStage = new Stage();
        aboutStage.setScene(new Scene(root));
        aboutStage.setTitle(bundle.getString("about.title.text"));
        aboutStage.initModality(Modality.APPLICATION_MODAL);
        aboutStage.setResizable(false);
        aboutStage.showAndWait();
    }
    
    @FXML 
    protected void closeApplicationActionPerformed(ActionEvent event) throws IOException{
        
        Stage addressBookStage = (Stage) menuBar.getScene().getWindow();
        addressBookStage.hide();
    }

    @FXML
    protected void newContactActionPerformed(ActionEvent event) throws IOException{
        presenter.newContact();
    }

    @FXML
    protected void deleteContactActionPerformed(ActionEvent event) throws IOException{
        presenter.removeCurrentContact();
    }

    @FXML
    protected void textFieldActionPerformed(ActionEvent event) throws IOException{
        presenter.textFieldActionPerformed(event);
    }

    private class TextFieldListener implements ChangeListener<Boolean>{

        private TextField textField;

        public TextFieldListener(TextField textField){
            this.textField = textField;
        }

        @Override
        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
            if(newValue){
                //Be prepared to save
                presenter.textFieldFocusGained(textField);

            }
            else{
                //Save data
                presenter.textFieldFocusLost(textField);
            }
        }
    }



}
