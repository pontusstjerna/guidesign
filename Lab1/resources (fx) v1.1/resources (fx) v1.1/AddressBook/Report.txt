Grupp 25
Aleksandar Babunovic & Pontus Stjernström

Problem:
*Man kan ange ogiltig email, postkod samt telefonnummer.
*Sparar inte kontakt om man direkt byter markerad kontakt (stödjer inte Changes in Midstream)

Förbättringar:
*Email borde kräva minst ett @ och en punkt.
*Postnr och telefonnummer borde endast tillåta siffror
*Krav på t.ex. namn
*Man skulle kunna ha med en funktion för att importera och exportera filerna.


